# Wall plume and stratification model

## Presentation

This model is presented in details in Caudwell *et al* 2016, *Convection at an isothermal wall in an enclosure and establishment of stratification*, J. Fluid Mech or in my Phd Thesis.

It computes the evolution of the flow and the ambient stratification that appear in a closed cavity when a vertical wall is heated at a given constant temperature. It is based on the Morton *et al* (1965) entrainment theory and Germeles (1975) numerical scheme as per the turbulent part, and on Worster and Leitch (1985) similarity solutions as per the laminar part.

The input parameters are:

* the height of the cavity `H`
* the global Rayleigh number `RaHc`
* the Prandtl number `Pr`

The model mainly computes :

* the plume volume flux `Q`
* the plume momentum flux `M`
* the plume buoyancy flux `F`
* the ambient density field `deltaE`

## Licence

This code is distributed under [CeCILL License](http://www.cecill.info/index.en.html) (GPL compatible licence).

## Installation

    git clone https://framagit.org/caudwell/wall-plume-stratification-model.git



## Run it

    cd wall-plume-stratification-model
    python wall_plume_model.py

The main file is called `wall_plume_model.py`. It uses the parameters given in its last part, and calls the two attached routines `ODE_MTT_solve.py` and `worster_leitch_solve.py`.

It possible to run the model as "fully turbulent" (`is_hybrid = False`) or as "hybrid" including the laminar modeling (`is_hybrid = True`).
It is possible to choose a constant (`is_alpha_var = False`) or a variable (`is_alpha_var = True`) entrainement coefficient.