#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
WALL PLUME & STRATIFICATION MODEL

Solving Ordinary Differential Equations for plume variables q and m at a given 
zeta step (MTT model)
"""
from __future__ import division

__author__ = "Tobit Caudwell"
__copyright__ = "Copyright 2016"
__license__ = "CeCILL v.1.2 (GPL compatible)"
__version__ = "1.0.0"
__maintainer__ = "Tobit Caudwell"
__email__ = "tobit <dot> caudwell <at> legi <dot> cnrs <dot> fr"


import numpy as np
from scipy import integrate


#zeta_1 = 0
#zeta_2 = 0.01
#dissip = 0
#ff = 6.57826116148351e+22
#q_init = 1e-12
#m_init = 1e-16
#alpha = 0.019

def ODE_MTT_solve(zeta_1, zeta_2, q_init, m_init, ff, alpha, dissip=0, nbPts=5000):

    def deriv(Y,zzeta): # return derivatives of the array X
        q = Y[0];
        m = Y[1];
         
        dqdz = alpha *m/q; 
        dmdz = q*ff/m - dissip*m/q; 
        return np.array([ dqdz, dmdz ])
       
    zeta = np.linspace(zeta_1,zeta_2, nbPts)
    Yinit = np.array([q_init,m_init]) # initial values
    Y = integrate.odeint(deriv,Yinit,zeta)
    
    return Y
