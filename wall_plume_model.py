#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
WALL PLUME & STRATIFICATION MODEL - main file

Parameters have to be defined at the end of the file.

This program computes plume and ambient stratification variables for the flow 
produced by a heated wall at a constant temperature in a closed cavity.
See "Convection at an isothermal wall in an enclosure and establishment of 
stratification", by Caudwell et al, 2016, J. Fluid Mech. for details.

"""
from __future__ import division

__author__ = "Tobit Caudwell"
__copyright__ = "Copyright 2016"
__license__ = "CeCILL v.1.2 (GPL compatible)"
__version__ = "1.0.0"
__maintainer__ = "Tobit Caudwell"
__email__ = "tobit <dot> caudwell <at> legi <dot> cnrs <dot> fr"

import numpy as np
import matplotlib.pyplot as plt

# Separate files
from worster_leitch_solve import worster_leitch_solve
from ODE_MTT_solve import ODE_MTT_solve


#%% Value of entrainment coefficient depending on the chosen law
def def_alpha(tau,is_hybrid,is_alpha_var):
    if is_hybrid:   # Hybrid model
        if is_alpha_var:
            alpha = 0.071*np.exp(-2.05*10**7*tau)+0.0039 # variable alpha, hybrid model
        else:
            alpha = 0.014
    else:           # Fully turbulent model
        if is_alpha_var:
            alpha = 0.065*np.exp(-1.55*10**7*tau)+0.0083 # variable alpha, fully turb.
        else:
            alpha = 0.019
    return alpha


#%% Computation of differential equations for plume variables
def compute_QMFi(i, zeta, q, m, f, deltaW, phi, deltaE, alpha):
    phi[i] = (deltaE[i-1] +deltaW)**(4/3) # Buoyancy flux at the wall
    f[i] = f[i-1] + q[i-1]*(deltaE[i] -deltaE[i-1]) + (zeta[i] -zeta[i-1])*phi[i]
    ff = f[i]

    # Computing of q(i) and m(i) by Runge Kutta method
    x = ODE_MTT_solve(zeta[i-1], zeta[i], q[i-1], m[i-1], ff, alpha)
    q[i] = x[-1, 0]
    m[i] = x[-1, 1]


#%% Computation of KQ0, KM0, KF0, KQ, KM, KF as functions of density exponent by 
# solving laminar boundary layer differential equations (Worster&Leitch 1985).
# These variables are needed in the laminar calculation of hybrid model.
def prelim_laminar(Pr):
    #%% Solving differential equations to get profiles for m=0 (homogeneous ambient)
    mm0=0
    xi_end = 20
    sol = worster_leitch_solve(xi_end, Pr, mm0)
    xi = np.linspace(0, xi_end, 300) # refined xi
    Sol = sol(xi) # refined solution (interpolation on xi)
       
    KQ0 =  Sol[0, -1]
    KM0 = np.trapz(Sol[1, :]**2,  xi)
    KF0 = np.trapz(Sol[1, :]*Sol[3, :], xi)

    #%% Variation of F(infty), max(F') and int(G) with m
    mm0=np.linspace(0, 0.5, 100)
    mm0 = np.extract(mm0!=1, mm0) # remove 1 value (if exists)
    
    KQ = np.zeros(mm0.shape, float)
    KM = np.zeros(mm0.shape, float)
    KF = np.zeros(mm0.shape, float)
    for i, mmi in enumerate(mm0):
        sol = worster_leitch_solve(xi_end, Pr, mmi)
        Sol = sol(xi) # refined solution (interpolation on xi)
    
        KQ[i] = Sol[0, -1]
        KM[i] = np.trapz(Sol[1, :]**2, xi)
        KF[i] = np.trapz(Sol[1, :]*Sol[3, :], xi)
#    plt.figure()
#    plt.plot(mm0, KQ, label='KQ')
#    plt.plot(mm0, KM, label='KM')
#    plt.plot(mm0, KF, label='KF')
#    plt.xlabel('m')
#    plt.legend()
    return mm0, KQ0, KM0, KF0, KQ, KM, KF



#%% Main computation of the model with iterations over space and time
def main_computation(N, nbZeta, threshold, deltaC, GrH, Pr, is_hybrid, is_alpha_var):

    #% Variables assignment and initialization
    ZETA = []       # Vertical axis
    DELTAe = []     # Ambient buoyancy (outside the plume)
    Q = []          # Plume volume flux
    M = []          # Plume momentum flux
    F = []          # Plume buoyancy flux
    Phi = []        # Buoyancy flux across the wall
    Tau = np.zeros(N+1, float) # Time array

    
    # Variables used at each time iteration
    zeta = np.linspace(0, 1, nbZeta)    # Current vertical axis
    q = 0*zeta                          # Current plume volume flux 
    m = 0*zeta                          # Current plume momentum flux
    f = 0*zeta                          # Current plume buoyancy flux
    deltaE = 0*zeta                     # Current ambient buoyancy
    phi = 0*zeta                        # Current buoyancy flux across the wall
    dtau = dtau0                        # Current time step
    
    if is_hybrid:  
        mm0, KQ0, KM0, KF0, KQ, KM, KF = prelim_laminar(Pr)
    
    # Variables needed for the hybrid model
    zeta_crit = 0 # Current position of lam/turb transition
    ZetaCrit = np.zeros(N, float)   # Array of zeta_crit
    mm = 0                         # Characteristic exponent of density profile (Worster&Leitch1985)
    MM = np.zeros(N, float)         # Array of mm
    p_correl = np.zeros(N, float)   # Array of correlation coefs for mm 
    stepQ = 0                       # Q step at lam/turb transition

    
    #%% %%%%%%%%%%% MAIN LOOP %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    for n in range(N): # loop on time
        # Initializatoin of instant variables
        q = 0*zeta
        m = 0*zeta
        f = 0*zeta
        phi = 0*zeta
        zeta[0] = 0
        
        alpha = def_alpha(Tau[n],is_hybrid,is_alpha_var) # Updating alpha value
        
        deltaE_mean = np.trapz(deltaE, zeta) # Averaged ambient density
        # "Wall buoyancy" (adjusted to take into account the slow change of 
        # temperature change in the aluminium wall)
        deltaW = 0.544*deltaC - 0.567*deltaE_mean 
    
        #%% FULLY TURBULENT case     
        if not is_hybrid: 
            lam = []
            # First zeta step
            q[0] = 10**-12 # Avoids zero value
            m[0] = 10**-16 # m0/q0 has to be very small
            
            for ii in range(3): # Iterative computation of first steps for better convergence
                compute_QMFi(1, zeta, q, m, f, deltaW, phi, deltaE, alpha) # 1=i+1
                # Correction of f after calculation of q
                f[1] = ((deltaE[1] + deltaW)**(4/3)*(zeta[1] - zeta[0]) + f[0]) \
                /(1+ (zeta[1] - zeta[0])/q[1]) 
            
            # Following zeta steps
            for i in range(sum(lam)+2, zeta.shape[0]):
                compute_QMFi(i, zeta, q, m, f, deltaW, phi, deltaE, alpha)
    
        #%% HYBRID case
        else:
            RaH = Kw**6/Pr**3 *deltaW # Update Rayleigh number
            
            # Variable Transition position
            GrH_transit = GrH*(1+deltaE_mean/deltaW) 
            zeta_crit = (10**9/GrH_transit)**(1/3)
            
            lambdaT0 = 1
            
            # Defining the different treatment zones
            lam = zeta<=zeta_crit   # Laminar zone
            lamHomog = lam * (zeta <= zeta[nbZeta-1]) # Laminar homogeneous zone 
            lamStrat = lam * (zeta > zeta[nbZeta-1])    # Laminar stratified zone
            
            #%% Laminar zone treatment %%%%%%%%%%%%%%%%%%%%

              # ~~~~~~ Homogeneous region ~~~~~~~
            q[lamHomog] = Kw**-3 *Pr    *(lambdaT0*RaH)**(1/4) *KQ0 *zeta[lamHomog]**(3/4)
            m[lamHomog] = Kw**-6 *Pr**2 *(lambdaT0*RaH)**(3/4) *KM0 *zeta[lamHomog]**(5/4)
            f[lamHomog] = Kw**-9 *Pr**4 *(lambdaT0*RaH)**(5/4) *KF0 *zeta[lamHomog]**(3/4)
            
              # ~~~~~~ Stratified region ~~~~~~~
            if sum(lam)>nbZeta:
                
                # Calculation of density characteristic exponent
                RaFront = RaH*zeta[nbZeta-1]**3 # Rayleigh number at the front
                rk = (lam==False).nonzero() # get indices of non laminar values
                lamStrat2 = lamStrat
                lamStrat2[rk[0]] = True # to include first non-laminar value in the following
                p = np.polyfit(np.log(RaH*zeta[lamStrat2]**3), np.log(1+deltaE[lamStrat2]/deltaW), 1)
                correl = np.corrcoef(np.log(1+deltaE[lamStrat2]/deltaW), \
                np.polyval(p, np.log(RaH*zeta[lamStrat2]**3))) # correlation coefficient
                p_correl[n] = correl[0, 1]
                mm = -p[0]
                lambdaTm = np.exp(p[1]/(1-mm))
#                plt.figure(figfit.number)
#                plt.plot(np.log(RaH*zeta[1:]**3), np.log(1+deltaE[1:]/deltaW), # '1:' to avoid 0 value
#                     np.log(RaH*zeta[lamStrat]**3), np.polyval(p, np.log(RaH*zeta[lamStrat]**3)), 0, 0, '+')
#                plt.grid()
                        
                KQm = np.interp(mm, mm0, KQ)
                KMm = np.interp(mm, mm0, KM)
                KFm = np.interp(mm, mm0, KF)

                if nbZeta!=1: # double layer
                    Cdelta = lambdaT0*lambdaTm**(mm-1) *RaFront**mm
                    lambdaXi = Cdelta**(-1/4) 
                    lambdaF = Cdelta**(1/4)*KM0/KQ0*KQm/KMm
                    lambdaG = Cdelta*KQm/KFm*((deltaE[nbZeta] - 0)/(lambdaT0*deltaW) + KF0/KQ0)
                    stepQ = Kw**-3*Pr*(lambdaT0*RaFront)**(1/4)*(KQ0 - Cdelta**(-1/4)*lambdaF*KQm) # Volume flux deficiency at front
                else: # entirely stratified
                    lambdaXi = 1 
                    lambdaF = 1
                    lambdaG = 1

                q[lamStrat] = Kw**-3 *Pr    *(lambdaTm*RaH)**(1/4*(1-mm)) *lambdaF            *KQm *zeta[lamStrat]**(3/4*(1-mm))
                m[lamStrat] = Kw**-6 *Pr**2 *(lambdaTm*RaH)**(3/4*(1-mm)) *lambdaF**2/lambdaXi*KMm *zeta[lamStrat]**(1/4*(5-9*mm))
                f[lamStrat] = Kw**-9 *Pr**4 *(lambdaTm*RaH)**(5/4*(1-mm)) *lambdaF*lambdaG    *KFm *zeta[lamStrat]**(3/4*(1-5*mm))
            
            phi[lam] = deltaE[lam]+deltaW
            MM[n] = mm
            
            #%% Turbulent zone treatment %%%%%%%%%%%%%%%%%%%%
            for i in range(sum(lam), zeta.shape[0]): 
                compute_QMFi(i, zeta, q, m, f, deltaW, phi, deltaE, alpha)

        #%% Velocity in the ambient
        u = -q
        u[nbZeta-1] = u[nbZeta-1]+stepQ
        
        #%% Storing current variables in global lists
        ZETA.append(zeta)
        DELTAe.append(deltaE)
        F.append(f)
        Q.append(q)
        M.append(m)
        Phi.append(phi)
        
        ZetaCrit[n] = zeta_crit
      
        #%% Next time step variables 

        # deltaE at the very top of the tank
        deltaE_bound = deltaE[-1] - f[-1]/q[-1] 
        
        # Advection in the ambient
        zeta = np.append(zeta + u*dtau, 1)
        deltaE = np.append(deltaE, deltaE_bound)
        
        # Time step (increasing as front velocity decreases)
        dtau = min(-3/4*(4/5)**(1/3)*alpha**(2/3)*deltaW**(4/9) / u[nbZeta-1]  * dtau0, 50*dtau0)
        Tau[n+1] = Tau[n] + dtau
        
        #%% Special corrections for code stability
        
        # Avoiding zeta negative values
        while zeta[1]<0:
            zeta = np.append(zeta[0], zeta[2:])
            deltaE = np.append(deltaE[0], deltaE[2:])
            nbZeta = max(nbZeta-1,3)
            print('== Removing negative zeta value == ')    
        
        # Managing the fact that front reaches the bottom (hybrid case)
        if zeta[nbZeta-1]<threshold:    # front below threshold
            zeta = np.append(zeta[0], zeta[nbZeta-2:])
            deltaE = np.append(deltaE[0], deltaE[nbZeta-2:])
            nbZeta = 3
            threshold = 0          # avoids executing these lines a second time
            print('=== Simplification of zeta below the front === ')
            
        #%% Console output
        print('n=%d\t alpha = %g \t dtau = %g \t zeta_crit = %1.3g \t mm = %g \t' % (n, alpha, dtau, zeta_crit, mm))
        


    return n, ZETA, Q, M, F, DELTAe, Phi, Tau, ZetaCrit


#%% Function to display variables
def display_curves(n, ZETA, Q, M, F, DELTAe, Tau, nbcurv, first):
    fig = {}    # dictionary containing figure names
    legLoc = {} # dictionary containing legend locations
    
    plt.close('all')
    fig['deltaE'] = plt.figure()    # Ambient buoyancy
    fig['f'] = plt.figure()         # Plume buoyancy flux
    fig['q'] = plt.figure()         # Plume volume flux
    fig['m'] = plt.figure()         # Plume momentum flux
    fig['w'] = plt.figure()         # Plume characteristic velocity
    fig['b'] = plt.figure()         # Plume characteristic width
    
    
    num = np.round(np.linspace(first, n-1, nbcurv)).astype(int)

    for k in num:
        leglabel = '$\\tau={0}$'.format(Tau[k])
        
        plt.figure(fig['deltaE'].number)
        plt.plot(DELTAe[k], ZETA[k], label=leglabel)
        plt.xlabel('$\delta_e$')
        legLoc['deltaE'] = 'lower left'
        
        plt.figure(fig['f'].number)
        plt.plot(F[k], ZETA[k], label=leglabel)
        plt.xlabel('$f$')
        legLoc['f'] = 'lower right'
        
        plt.figure(fig['q'].number)
        plt.plot(Q[k], ZETA[k], label=leglabel)
        plt.xlabel('$q$')
        legLoc['q'] = 'lower right'
        
        plt.figure(fig['m'].number)
        plt.plot(M[k], ZETA[k], label=leglabel)
        plt.xlabel('$m$')
        legLoc['m'] = 'lower right'
        
        plt.figure(fig['w'].number)
        plt.plot(M[k]/Q[k], ZETA[k], label=leglabel) # Plume velocity
        plt.xlabel('$w$')
        legLoc['w'] = 'lower right'
        
        plt.figure(fig['b'].number)
        plt.plot(Q[k]**2/M[k], ZETA[k], label=leglabel) # Plume width
        plt.xlabel('$b$')
        legLoc['b'] = 'lower right'
        
        
    for i in fig:
        plt.figure(fig[i].number)
        plt.ylabel('$\zeta$')
        plt.legend(loc=legLoc[i], frameon=False)
        
    plt.show()


#%%
if __name__ == '__main__':

    #%% Parameters %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    # Model type parameters
    is_hybrid = True        # False for Fully turbulent model, True for Hybrid model
    is_alpha_var = True     # Constant or variable entrainment coefficient
    
    # Computation parameters
    N=600               # Number of time steps
    nbZeta = 100        # Number of elements for zeta at the beginning
    dtau0 = 2*10**-10   # Initial time step
    threshold = 0.01    # Threshold to be reached by the front before simplification of zeta
    
    # Parameters specific to the experiment
    hg = 300            # (W.m-2.K-1) Heat transfer coefficient (global)
    hc = 500            # (W.m-2.K-1) Heat transfer coefficient (compartment)
    H = 0.551           # (m) Height of the tank
    RaHc = 1.35*10**11  # Global Rayleigh number based on lateral compartment temperature
    Pr = 5.5            # Prandtl number

    # Display parameters
    nbcurv = 5          # Number of curves to plot
    firstcurv = 0       # Time index of the first curve to plot

    
    #%% Variables needed by the model %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    RaH0 = (1-hg/hc)*RaHc       # Global Rayleigh number based on initial wall temperature
    GrH = RaH0/Pr               # Global Grashof number based on initial wall temperature
    Kw = 0.120                  # Heat transfer constant (from Tsuji&Nagano 1988)
    deltaC = Kw**-6*RaHc*Pr**3  # 'Wall buoyancy' based on lateral compartment temperature
    
    #%% Computation
    n, ZETA, Q, M, F, DELTAe, Phi, Tau, ZetaCrit = \
    main_computation(N, nbZeta, threshold, deltaC, GrH, Pr, is_hybrid, is_alpha_var)

    #%% Graphs output
    display_curves(n, ZETA, Q, M, F, DELTAe, Tau, nbcurv, firstcurv)
