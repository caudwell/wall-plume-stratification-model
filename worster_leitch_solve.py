#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
WALL PLUME & STRATIFICATION MODEL

Solving boundary layer equations given in "Laminar free convection in confined
regions" by Worster & Leitch, 1985, J. Fluid Mech.

"""
from __future__ import division

__author__ = "Tobit Caudwell"
__copyright__ = "Copyright 2016"
__license__ = "CeCILL v.1.2 (GPL compatible)"
__version__ = "1.0.0"
__maintainer__ = "Tobit Caudwell"
__email__ = "tobit <dot> caudwell <at> legi <dot> cnrs <dot> fr"


import scikits.bvp_solver
import numpy as np

def worster_leitch_solve( xi_end=20, Pr=5, m=0):

    # Main function 
    def function(xi, X):
        F = X[0]
        dFdz = X[1]
        dFdz2 = X[2]
        G = X[3]
        dGdz = X[4];
            
        dFdz3 = -( 1/Pr*(3/4*(1-m)*F*dFdz2 - 1/2*(1-3*m)*dFdz**2) + G ) # equation 1
        dGdz2 = -( 3/4*(1-m)*F*dGdz - 3*m*(1-G)*dFdz )                  # equation 2
        return np.array([dFdz,
                            dFdz2,
                            dFdz3,
                            dGdz,
                            dGdz2])   
                            
    # Estimation of the final function
    def guess(X):
    
        return np.array([-2*np.exp(-X)+np.exp(-2*X),        # F
                            2*np.exp(-X)-2*np.exp(-2*X),    # F'
                            -2*np.exp(-X)+4*np.exp(-2*X),   #F''
                            np.exp(-2*X),                   # G
                            -2*np.exp(-2*X)])               # G'
                            
    
    # Definition of boundary equations (L for left, R for right)
    def boundary_conditions(xiL,xiR):
    
        return (np.array([xiL[0] - 0,           # F(0) = 0
                             xiL[1] - 0,        # F'(0) = 0
                             xiL[3] - 1]),      # G(0) = 1 
                np.array([xiR[1] - 0,           # F'(end) = 0
                             xiR[3] - 0]))      # G(end) = 0
                              
    
    # Problem definition
    problem = scikits.bvp_solver.ProblemDefinition(num_ODE = 5,
                                          num_parameters = 0,
                                          num_left_boundary_conditions = 3,
                                          boundary_points = (0, xi_end),
                                          function = function,
                                          boundary_conditions = boundary_conditions)
    
    solution = scikits.bvp_solver.solve(problem,
                                solution_guess = guess)
    
    return solution

#%% In the case the script is executed alone
if __name__ == "__main__" :
    xi_end=20
    Pr=5
    m=0
    sol = worster_leitch_solve(xi_end, Pr, m)
    
    xi = np.linspace(0,xi_end, 100)
    Sol = sol(xi) # evaluation of the solution on xi points

    # Plotting solution for F' and G
    import pylab
    pylab.plot(xi, Sol[1,:],'-')
    pylab.plot(xi, Sol[3,:],'-')
    pylab.show()